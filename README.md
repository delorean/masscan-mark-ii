# masscan - mark II

masscan stripped of its trackers. evades fingerprinting by threat intelligence firms, service providers, and everyone in between.

# Why?

the original masscan contains sneaky values in its headers and payloads intended to identify the traffic as belonging to masscan.

this makes it trivial for threat intelligence agencies, isp's, and anyone else to fingerprint masscan traffic and generate alerts/tags/abuse reports accordingly.

# Installation

**dependencies**: gcc, make, git

```
git clone https://git.supernets.org/delorean/masscan-mark-ii
cd masscan-mark-ii
make
```

this will yield a **'masscan'** binary in *./bin*

 to automatically install it into your path (Linux),
 
 ```
 make install
 ```

# Usage

```
masscan -h
```

refer to [masscan's usage](https://github.com/robertdavidgraham/masscan/tree/master?tab=readme-ov-file#usage)